import { BrowserRouter as Router} from 'react-router-dom';
import { Header } from './components/Header';
import { Footer } from './components/Footer';
import { GlobalStyle } from './styles/global';
import Routes from './routes'

export default function App() {
  return (
    <>
   
     <GlobalStyle/>
      <Header/>
      <Router>
    <Routes/>
    </Router>
      <Footer/>
      
    </>
  );
}

