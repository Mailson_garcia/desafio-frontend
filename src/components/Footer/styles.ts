import styled from 'styled-components';

export const Container = styled.footer`
  background: var(--whithe);
  box-shadow: 0px -4px 4px rgba(47, 37, 68, 0.15);
  margin: 0 auto;
  height: 2.25rem;
  padding: 1rem;
  text-align: center;
  color: var(--text-body);
  position: absolute;
  width: 100%;
  bottom: 0;

  


`;