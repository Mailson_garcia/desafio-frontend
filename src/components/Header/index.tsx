import React, {useState}  from 'react';
import { Link } from 'react-router-dom';
import api from '../../services/apí';
import logoImg from '../../assets/logo.svg';
import {BsSearch} from 'react-icons/bs';
import { Container, Content } from './styles';


interface User {
   total_coumt: number;
   items: {
     login: string;
     avatar_url: string;
     url: string;
     score: string;

   }


}

export function Header () {
  const [newSearch, setNewSearch] = useState('');
  const [users, setUsers] = useState<User[]>([]);


  async function handleAddSearch(): Promise<void> {
    const response = await api.get<User>(`search/users?q=${newSearch}`);
    const resultsSearch = response.data;
    
    setUsers([resultsSearch]);

  }



  return (
    <Container>
      <Content>
      <img src={logoImg} alt="The Git Search" />
      
        <input 
        value={newSearch}
        onChange={(e) => setNewSearch(e.target.value)}
        type="text" placeholder="Pesquisar"/>
        <button type='button'  onClick={handleAddSearch}><BsSearch color="#FFF" size={14}/>
        </button>
        
      
      
      </Content>
    </Container>
  )
}