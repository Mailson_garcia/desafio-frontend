import styled from 'styled-components';

export const Container = styled.header`
  background: var(--whithe);
  box-shadow: 0px 4px 4px rgba(47, 37, 68, 0.15);

`;


export const Content = styled.div`
  max-width: 1120px;
  margin: 0 auto;
  padding: 1rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  
  

  input {
    width: 100%;
    height: 2.25rem;
    border: 1px solid #E1DFE0;
    box-sizing: border-box;
    border-radius: 4px 0px 0px 4px;
    margin-left: 2rem;
    padding-left: 0.625rem;
    font-size: small;

    &::placeholder {
      color: var(--light-accent);
    }

    &:focus {
      font-size: medium;
      color: var(--light-accent);
      outline: 0px;
    }
    
  }

  button {

    height: 2.25rem;
    width: 2.25rem;
    background: var(--fuchsia-blue);
    border-radius: 0px 4px 4px 0px;
    box-shadow: -2px 0px 4px rgba(0, 0, 0, 0.25);
    border: 0;

    &:hover {
      filter: brightness(0.8);
    }



    
  }
`;

export const Form = styled.form`

`;
