import styled from 'styled-components';
import logoImg from '../../assets/Logo-shadow.svg'


export const Container = styled.main`
display: flex;
min-height: 80vh; 
position: relative;
background: var(--background);



`;

export const Background = styled.div`
  flex: 1;
  background: url(${logoImg})  no-repeat center;
  background-size: 45.625rem 25.625rem;
  @media (max-width: 1880px){
    background-size: 40.375rem 19.375rem;
  }

  @media (max-width: 720px){
    background-size: 34.125rem 27.875rem;
  }
  
`;