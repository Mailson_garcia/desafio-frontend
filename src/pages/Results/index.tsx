import { useState, useEffect}  from 'react';
import {  Container, Content} from './styles';
import avatar  from '../../assets/Avatar.png';


const Results: React.FC = () => {

const [results, setResults] = useState([]);

useEffect (() => {
  fetch('https://api.github.com/search/users?q=mailsongarcia')
  .then(response => response.json())
  .then(data => setResults(data))
}, [])

  return (
    <Container>
      <h1>Resultados para: Paytime</h1>
      <Content>
      

      <div>
        <img src={avatar} alt="avatar" />
        <strong>Milena Melo</strong>
        <a href="https://github.com/mailsongarcia">https://github.com/mailsongarcia</a>
        <p>Score: 1.00</p>
        <button>Ver mais</button>
      </div>
      <div>
        <img src={avatar} alt="avatar" />
        <strong>Milena Melo</strong>
        <a href="https://github.com/mailsongarcia">https://github.com/mailsongarcia"</a>
        <p>Score: 1.00</p>
        <button>Ver mais</button>
      </div>
      <div>
        <img src={avatar} alt="avatar" />
        <strong>Milena Melo</strong>
        <a href="https://github.com/mailsongarcia"></a>
        <p>Score: 1.00</p>
        <button>Ver mais</button>
      </div>
      <div>
        <img src={avatar} alt="avatar" />
        <strong>Milena Melo</strong>
        <a href="https://github.com/mailsongarcia"></a>
        <p>Score: 1.00</p>
        <button>Ver mais</button>
      </div>

      </Content>
      
    </Container>
  )
}

export default Results;
