import styled from 'styled-components';



export const Container = styled.main`

  h1{
    max-width: 60.938rem;
    font-weight: 400;
    margin: 0 auto;
    margin-top: 3.125rem;
    font-size: 1.125rem;
    color: var(--text-title);
    line-height: 1.318rem;
    border-bottom: solid 1px #E1DFE0;
    margin-bottom: 0.625rem;
  }


`;

export const Content = styled.main`
  max-width: 60.938rem;
  margin: 0 auto;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 0.625rem;

  


div {
  background: var(--whithe);
  border-radius: 0.625rem;
  width: 14.375rem;
  height: 17.063rem;
  box-shadow: 0px 0px 6px rgba(47, 37, 68, 0.25);
  font-size: 0.75rem;
  line-height: 0.879rem;

  img {
    width: 14.375rem;
    height: 8.125rem;
    

  }

  strong{
    font-size: 1.125rem;
    font-weight: 400;
    line-height: 1.318rem;
    color: var(--text-title);
    display: block;
    margin: 0.625rem 0rem 0.25rem 0.625rem;
  }

  a{
    color: var(--cyan);
    margin: 0rem 0rem 0.25rem 0.625rem;
  }

  p{
    color: var(--text-body);
    margin: 0rem 0rem 0.25rem 0.625rem;

  }

  button {
    width: 13.125rem;
    height: 2.25rem;
    background: var(--fuchsia-blue);
    border-radius: 1.25rem;
    border: 0;
    margin: 1.25rem 0.625rem;
    font-weight: 500;
    color: var(--whithe);
    text-transform: uppercase;

    &:hover {
      filter: brightness(0.9);
    }


  }
}


`;

  
