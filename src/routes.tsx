import React from 'react';
import { Switch, Route} from "react-router-dom";
import Dashboard from './pages/Dashboard';
import Results from './pages/Results';


 
const Routes: React.FC = ()=> (
    <Switch>
      <Route path="/" exact component={Results}/>
    </Switch>
);

export default Routes;